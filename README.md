Tianhe integration/staging tree
================================

Copyright (c) 2009-2013 Bitcoin Developers
Copyright (c) 2014 Vertcoin Developers
Copyright (c) 2015 Tianhe Developers

What is Tianhe?
----------------

Tianhe is a cryptocoin using Lyra2REv2 as a proof-of-work algorithm.

 - 1 minute block targets
 - 25 coins per block
 - Difficulty retargeting with Kimoto's Gravity Well
 - Stealth addresses (experimental, use at own risk)


Ports for client connectivity
-----------------------------
 
Mainnet RPC 7030
Mainnet P2P 7031

Testnet RPC 17030
Testnet P2P 17031

License
-------

Tianhe is released under the terms of the MIT license. See `COPYING` for more
information or see http://opensource.org/licenses/MIT.

Development process
-------------------

Will be developed further.

Compiling
---------

Dependencies needed are exactly same as in Vertcoin;

	sudo apt-get install automake autoconf libtool build-essential libgmp-dev

Also, we need to install the secp256k1 module:
	
	cd /opt
	git clone https://github.com/bitcoin/secp256k1.git
    cd secp256k1
    git checkout 4c63780710351ffcc52341497e3ca0e518fbad79
	sudo ./autogen.sh
	sudo ./configure
	sudo make
	sudo make install (optional)

Update LD cache so the system knows about the freshly installed secp256k1 existence,

	sudo ld-config

Makefiles for the core code are in `src/`. To compile and run them, type in Tianhe directory:

    cd src
	./autogen.sh (if building from git repository)
	./configure
	make -f makefile.unix
	strip tianhed
	
To run freshly compiled daemon, type:
	
	./tianhed

Makefiles for the Qt client are in `src/`. To compile and run them:

	cd src
	./autogen.sh (if building from git repository)
	./configure
    make -f makefile.linux-mingw
	strip tianhe-qt
	
To run freshly compiled Qt client, type:
	
    ./tianhe-qt

